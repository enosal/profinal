proFinal
========

My final for Java is Brick Breaker/Breakout game. Use the left and right arrow keys to move the bar from side to side. Completing a level advances to the next level (up to 10 levels). There is also the option to start from level 10.

Java, Fall 2015