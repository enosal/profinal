package edu.uchicago.cs.java.finalproject.mvc.controller;

import edu.uchicago.cs.java.finalproject.mvc.model.*;
import edu.uchicago.cs.java.finalproject.mvc.view.GamePanel;
import edu.uchicago.cs.java.finalproject.sounds.Sound;

import javax.sound.sampled.Clip;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

// ===============================================
// == This Game class is the CONTROLLER
// ===============================================

public class Game implements Runnable, KeyListener {

	// ===============================================
	// FIELDS
	// ===============================================

	public static final Dimension DIM = new Dimension(1000, 800); //the dimension of the game.
	private GamePanel gmpPanel;
	public static Random R = new Random();
	public final static int ANI_DELAY = 42; // milliseconds between screen
											// updates (animation)
	private Thread thrAnim;
	private int nLevel = 1;
	private int nTick = 0;

	private boolean bMuted = true;

	private final static int POINTS = 100;

	private final int PAUSE = 80, 	// p key
			QUIT = 81, 				// q key
			LEFT = 37, 				// left arrow
			RIGHT = 39, 			// right arrow
			START = 83, 			// s key
			BALL = 32, 				// space key
			MUTE = 77, 				// m-key mute
			TEN = 84; 				//t-key for level ten


	private Clip clpThrust;
	private Clip clpMusicBackground;

	//special levels
	private boolean bLevelTen = false;

	private static final int MAX_LEVEL = 10;

	//Draw blocks from
	private static final int NUM_BLOCKS_IN_ROW = 13;
	private static final int topDraw = 60;
	private static final int rowSpace = 65;
	private static final int leftDraw = 150;
	private static final int colSpace = 30;


	// ===============================================
	// ==CONSTRUCTOR
	// ===============================================

	public Game() {

		gmpPanel = new GamePanel(DIM);
		gmpPanel.addKeyListener(this);
		clpThrust = Sound.clipForLoopFactory("whitenoise.wav");
		clpMusicBackground = Sound.clipForLoopFactory("music-background.wav");
	

	}

	// ===============================================
	// ==METHODS
	// ===============================================

	public static void main(String args[]) {
		EventQueue.invokeLater(new Runnable() { // uses the Event dispatch thread from Java 5 (refactored)
					public void run() {
						try {
							Game game = new Game(); // construct itself
							game.fireUpAnimThread();

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	// Called when user presses 's'
	private void startGame() {
		Cc.getInstance().clearAll();
		Cc.getInstance().initGame();
		Cc.getInstance().setLevel(0);
		Cc.getInstance().setPlaying(true);
		Cc.getInstance().setPaused(false);
		if (!bMuted)
		 clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
	}

	private void fireUpAnimThread() { // called initially
		if (thrAnim == null) {
			thrAnim = new Thread(this); // pass the thread a runnable object (this)
			thrAnim.start();
		}
	}

	// implements runnable - must have run method
	public void run() {

		// lower this thread's priority; let the "main" aka 'Event Dispatch'
		// thread do what it needs to do first
		thrAnim.setPriority(Thread.MIN_PRIORITY);

		// and get the current time
		long lStartTime = System.currentTimeMillis();

		// this thread animates the scene
		while (Thread.currentThread() == thrAnim) {
			tick();
			gmpPanel.update(gmpPanel.getGraphics()); // update takes the graphics context we must 
														// surround the sleep() in a try/catch block
														// this simply controls delay time between 
														// the frames of the animation

			//check for ball-block collisions
			checkCollisions();

			//check if the level is clear (no more blocks)
			if (getLevelTen()) {
				Cc.getInstance().setLevel(MAX_LEVEL);
				spawnBlocks(NUM_BLOCKS_IN_ROW, Cc.getInstance().getLevel());
				setLevelTen(false);
			}
			else {
				checkNewLevel();
			}


			try {
				// The total amount of time is guaranteed to be at least ANI_DELAY long.  If processing (update) 
				// between frames takes longer than ANI_DELAY, then the difference between lStartTime - 
				// System.currentTimeMillis() will be negative, then zero will be the sleep time
				lStartTime += ANI_DELAY;
				Thread.sleep(Math.max(0,
						lStartTime - System.currentTimeMillis()));
			} catch (InterruptedException e) {
				// just skip this frame -- no big deal
				continue;
			}
		} // end while
	} // end run



	private void checkCollisions() {
		Point pntBallCenter, pntBlockCenter;
		double dBallRadiux, dBlockRadiux;

		Movable movBall = Cc.getInstance().getBall();
		Ball ballBall = (Ball) movBall;

		if (movBall == null) {
			return;
		}
		for (Movable movBlock : Cc.getInstance().getMovBlocks()) {

			Block blockBlock = (Block) movBlock;

			pntBallCenter = movBall.getCenter();
			pntBlockCenter = movBlock.getCenter();
			dBallRadiux = movBall.getRadius();
			dBlockRadiux = movBlock.getRadius();

			//detect collision
			if (Cc.getInstance().getDetectCollisions()) {
				if (pntBallCenter.distance(pntBlockCenter) < (dBallRadiux + dBlockRadiux) ) {
					Cc.getInstance().setDetectCollisions(false);
//					System.out.println("COLLISION!");
//					System.out.println("Distance: " + pntBallCenter.distance(pntBlockCenter));
//					System.out.println("Combined Radii Distance: " + (dBallRadiux + dBlockRadiux));
//					System.out.println();

					Cc.getInstance().getOpsList().enqueue(movBlock, CollisionOp.Operation.REMOVE);

//					if (ballBall.getAngle() <= ballBall.getMIN_ANGLE() && ballBall.getAngle() >= (ballBall.getMIN_ANGLE() + ballBall.getMAX_ANGLE())) {
//					}


					if (pntBallCenter.x < pntBlockCenter.x - blockBlock.getBlockWidth()/2 || pntBallCenter.x > pntBlockCenter.x + blockBlock.getBlockWidth()/2) {
					ballBall.hitBlockLR();
					}
					if (pntBallCenter.y < pntBlockCenter.y - blockBlock.getBlockHeight()/2 || pntBallCenter.y > pntBlockCenter.y + blockBlock.getBlockHeight()/2) {
					ballBall.hitBlockUD();
					}

					Cc.getInstance().setScore(Cc.getInstance().getScore() + POINTS);
					Cc.getInstance().getOpsList().enqueue(movBlock, CollisionOp.Operation.REMOVE);

					//Sound is from http://www.sxtgedu.net/download/sounds/buttons/click_01.wav
					Sound.playSound("click_01.wav");

				}//end if
			}
		}//end for


		//we are dequeuing the opsList and performing operations in serial to avoid mutating the movable arraylists while iterating them above
		while(!Cc.getInstance().getOpsList().isEmpty()){
			CollisionOp cop =  Cc.getInstance().getOpsList().dequeue();
			Movable mov = cop.getMovable();
			CollisionOp.Operation operation = cop.getOperation();

			switch (mov.getTeam()){
				case BLOCK:
					if (operation == CollisionOp.Operation.ADD){
						Cc.getInstance().getMovBlocks().add(mov);
					} else {
						Cc.getInstance().getMovBlocks().remove(mov);
					}

					break;
				case FRIEND:
					if (operation == CollisionOp.Operation.ADD){
						Cc.getInstance().getMovFriends().add(mov);
					} else {
						Cc.getInstance().getMovFriends().remove(mov);
					}
					break;

				case DEBRIS:
					if (operation == CollisionOp.Operation.ADD){
						Cc.getInstance().getMovDebris().add(mov);
					} else {
						Cc.getInstance().getMovDebris().remove(mov);
					}
					break;

			}

		}
		//a request to the JVM is made every frame to garbage collect, however, the JVM will choose when and how to do this
		System.gc();
		
	}//end meth


	//this method spawns rows of block
	private void spawnBlocks(int nRowBlocks, int nColBlocks) {
		for (int nR = 0; nR < nRowBlocks; nR++) {
			for (int nC = 0; nC <= nColBlocks; nC++) {
				//Each row gets a different color block
				Cc.getInstance().getOpsList().enqueue(new Block(getBlockColor(nC), new Point(topDraw + rowSpace * nR, leftDraw + colSpace * nC)), CollisionOp.Operation.ADD);
			}
		}
	}



	//some methods for timing events in the game,
	//such as the appearance of UFOs, floaters (power-ups), etc.
	public void tick() {
		if (nTick == Integer.MAX_VALUE)
			nTick = 0;
		else
			nTick++;
	}

	public int getTick() {
		return nTick;
	}




	private boolean isLevelClear(){
		boolean bBlockFree = true;
			for (Movable moveBlock : Cc.getInstance().getMovBlocks()) {
				if (moveBlock instanceof Block) {
					bBlockFree = false;
					break;
				}
			}
			return bBlockFree;
		}

	private void checkNewLevel(){
		if (isLevelClear()){
			if (Cc.getInstance().getBar() != null) {
				spawnBlocks(NUM_BLOCKS_IN_ROW, Cc.getInstance().getLevel());
			}
			Cc.getInstance().setLevel(Cc.getInstance().getLevel() + 1);
			if (Cc.getInstance().getBall() != null) {
				Cc.getInstance().getBall().stopMoving();
			}
		}
	}

	private Color getBlockColor(int nNum) {
		Color col = Color.gray;
		switch (nNum) {
			case 0: col = Color.magenta; break;
			case 1: col = new Color(0x7D26CD); break;
			case 2: col = Color.blue; break;
			case 3: col = Color.cyan; break;
			case 4: col = Color.green; break;
			case 5: col = Color.yellow; break;
			case 6: col = new Color(0xFF8000); break;
			case 7: col = Color.red; break;
			case 8: col = Color.pink; break;
			case 9: col = Color.white; break;
			default: break;
		}
		return col;
	}

	public void setLevelTen(boolean bTen) {
		bLevelTen = bTen;
	}

	public boolean getLevelTen() {
		return bLevelTen;
	}


	// Varargs for stopping looping-music-clips
	private static void stopLoopingSounds(Clip... clpClips) {
		for (Clip clp : clpClips) {
			clp.stop();
		}
	}



	// ===============================================
	// KEYLISTENER METHODS
	// ===============================================

	@Override
	public void keyPressed(KeyEvent e) {
		Bar bar = Cc.getInstance().getBar();
		Ball ball = Cc.getInstance().getBall();
		int nKey = e.getKeyCode();
		// System.out.println(nKey);

		if (nKey == START && !Cc.getInstance().isPlaying()) {
			startGame();
		}

		if (nKey == TEN && !Cc.getInstance().isPlaying()) {
			setLevelTen(true);
			startGame();
		}

		if (bar != null) {

			switch (nKey) {
			case PAUSE:
				Cc.getInstance().setPaused(!Cc.getInstance().isPaused());
				if (Cc.getInstance().isPaused())
					stopLoopingSounds(clpMusicBackground, clpThrust);
				else
					clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
				break;
			case QUIT:
				System.exit(0);
				break;
			case BALL:
				ball.startMoving();
				break;
			case LEFT:
				bar.moveLeft();
				break;
			case RIGHT:
				bar.moveRight();
				break;
			default:
				break;
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		Bar bar = Cc.getInstance().getBar();
		Ball ball = Cc.getInstance().getBall();
		int nKey = e.getKeyCode();
		System.out.println(nKey);

		if (bar != null) {
			switch (nKey) {
			case LEFT:
				bar.stopMoving();
				break;
			case RIGHT:
				bar.stopMoving();
				break;
			case MUTE:
				if (!bMuted){
					stopLoopingSounds(clpMusicBackground);
					bMuted = !bMuted;
				} 
				else {
					clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
					bMuted = !bMuted;
				}
				break;
				
				
			default:
				break;
			}
		}
	}

	@Override
	// Just need it b/c of KeyListener implementation
	public void keyTyped(KeyEvent e) {
	}

}


