package edu.uchicago.cs.java.finalproject.mvc.model;

import java.awt.*;

/**
 * Created by Eryka on 11/23/2015.
 */
public class Debris implements Movable {

    private static final int EXPIRE = 100;
    private int mExpire;
    private Point mCenter;

    public Debris(Point point) {
        mCenter = point;
        mExpire = EXPIRE;
    }

    @Override
    public void move() {
        if (mExpire == 0) {
            //this is the object on the heap at runtime- the debric
            Cc.getInstance().getOpsList().enqueue(this, CollisionOp. Operation.REMOVE);
        }

        mExpire--;
    }

    @Override
    public void draw(Graphics g) {
        g.setColor(Color.RED);
        g.fillOval(mCenter.x, mCenter.y, mExpire, mExpire );
    }

    @Override
    public Team getTeam() {
        return Team.DEBRIS;
    }



    //Collision Detection
    @Override
    public Point getDrawFrom() {
        return null;
    }

    @Override
    public double getRadius() {
        return 0;
    }


    public Point getCenter() {
        return new Point();
    }

}
