package edu.uchicago.cs.java.finalproject.mvc.model;
import java.awt.*;

/**
 * Block objects are to be hit by the ball. NOTE: Block objects are not (game-wise) moveable construct methods even though Movable can be implemented on them
 */

public class Block extends Shape {

	// ==============================================================
	// FIELDS
	// ==============================================================

	//Center point
	private Point pntCenter;

	//size of block
	private final int BLOCK_WIDTH = 60;
	private final int BLOCK_HEIGHT = 20;

	//Corners
	private Point pntTopLeft;
	private Point pntTopRight;
	private Point pntBottomLeft;
	private Point pntBottomRight;

	// ==============================================================
	// Constructor
	// ==============================================================

	//Construct block of color, at pnt, with strength hits needed to eliminate it
	public Block(Color color, Point pntTL){
		//call Shape constructor
		super();

		//Set team
		setTeam(Team.BLOCK);

		//Set color, center
		setColor(color);
		setDrawFrom(pntTL);

		//Set corner Points
		pntTopLeft = pntTL;
		pntTopRight = new Point(pntTL.x + BLOCK_WIDTH, pntTL.y);
		pntBottomLeft = new Point(pntTL.x, pntTL.y + BLOCK_HEIGHT);
		pntBottomRight = new Point(pntTL.x + BLOCK_WIDTH, pntTL.y + BLOCK_HEIGHT);

		//Center point
		pntCenter = new Point();

		//Set TopLeft and Center points
		setDrawFrom(pntTopLeft);
		setCenter(getDrawFrom(), BLOCK_WIDTH, BLOCK_HEIGHT);
	}

	// ==============================================================
	// Methods
	// ==============================================================

	//getters for block width/height
	public int getBlockWidth() {return BLOCK_WIDTH;}

	public int getBlockHeight() {return BLOCK_HEIGHT;}


	@Override
	public void draw(Graphics g) {
		g.setColor(getColor());
		g.fillRect(getDrawFrom().x, getDrawFrom().y, BLOCK_WIDTH, BLOCK_HEIGHT);
	}

	//blocks do not move
	public void move() {
		setDeltaX(0);
		setDeltaY(0);
	}

	//based on http://math.stackexchange.com/questions/99203/finding-the-radius-of-a-rectangle-based-on-a-given-x-y-coordinate
	public double getRadius() {
		//get ball angle
		double dBallAngle = Cc.getInstance().getBallAngle();
		double rad = 0;

		//Hits top or bottom
		if (dBallAngle <= -45 && dBallAngle >= -135) {
			rad = ((BLOCK_WIDTH/2) / Math.sin(Math.toRadians(dBallAngle)));
		}
		//Hits right or left
		else if ((dBallAngle > -45 && dBallAngle <= 0) || (dBallAngle < -135 && dBallAngle > 180)) {
			rad = ((BLOCK_WIDTH/2) / Math.cos(Math.toRadians(dBallAngle)));
		}
		return Math.abs(rad);
	}

	public void setCenter(Point pnt, int width, int height) {
		pntCenter.x = pnt.x + width/2;
		pntCenter.y = pnt.y + height/2;
	}

	public Point getCenter() { return pntCenter; }

}
