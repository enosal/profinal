package edu.uchicago.cs.java.finalproject.mvc.model;

import edu.uchicago.cs.java.finalproject.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;

/**
 * Bar is the moving paddle used to bounce the ball. It can be moved left & right. Space bar puts the ball into play
 */

public class Bar extends Shape {

	// ==============================================================
	// FIELDS 
	// ==============================================================

	//increment for left & right movement
	final int DEGREE_STEP = 10;

	//moving booleans
	private boolean bMovingRight = false;
	private boolean bMovingLeft = false;

	//TopLeft and Center point
	private Point pntCenter;
	private Point pntTopLeft;

	//Bar position and dimension info
	private int nWidth;
	private static final int BAR_WIDTH = 100;
	private static final int BAR_HEIGHT = 25;

	// ==============================================================
	// CONSTRUCTOR 
	// ==============================================================
	
	public Bar() {
		//call Shape constructor
		super();

		//set team
		setTeam(Team.FRIEND);

		//Points
		pntCenter = new Point();
		pntTopLeft = new Point((Game.DIM.width / 2) - (BAR_WIDTH / 2), Game.DIM.height - 2 * BAR_HEIGHT);

		//Set TopLeft and Center points
		setDrawFrom(pntTopLeft);
		setCenter(getDrawFrom(), BAR_WIDTH, BAR_HEIGHT);

		//this is the width of the bar
		setWidth(BAR_WIDTH);
	}
	
	
	// ==============================================================
	// METHODS 
	// ==============================================================

	//to set move left
	public void moveLeft() {
		bMovingLeft = true;
	}

	//to set move right
	public void moveRight() {
		bMovingRight = true;
	}

	//to stop moving
	public void stopMoving() {
		bMovingRight = false;
		bMovingLeft = false;
	}


	//Getter & Setters for Width of Bar, getter for height of bar
	public int getHeight() {
		return BAR_HEIGHT;
	}

	public int getWidth() {
		return nWidth;
	}

	public void setWidth(int w) {nWidth = w;}


	//Override
	public void move() {
		super.move();
		setCenter(getDrawFrom(), BAR_WIDTH, BAR_HEIGHT);

		if (bMovingLeft) {
			if (getCenter().x - getWidth()/2 <= 0 && bMovingLeft) {
				setDrawFrom(new Point(0, getDrawFrom().y));
			}
			setDrawFrom(new Point(getDrawFrom().x - DEGREE_STEP, getDrawFrom().y));
		}
		if (bMovingRight) {
			if ((getCenter().x + getWidth()/2) >= (Game.DIM.width) && bMovingRight) {
				setDrawFrom(new Point((Game.DIM.width - BAR_WIDTH), getDrawFrom().y));
			}
			setDrawFrom(new Point(getDrawFrom().x + DEGREE_STEP, getDrawFrom().y));
		}

	} //end move

	public void draw(Graphics g) {g.fillRect(getDrawFrom().x, getDrawFrom().y,  getWidth(), BAR_HEIGHT);}

	public void setCenter(Point pnt, int width, int height) {
		pntCenter.x = pnt.x + width/2;
		pntCenter.y = pnt.y + height/2;
	}

	public Point getCenter() { return pntCenter; }


}
