package edu.uchicago.cs.java.finalproject.mvc.model;
import edu.uchicago.cs.java.finalproject.sounds.Sound;

import java.util.ArrayList;
import java.util.List;


public class Cc {

	private int nNumBall;
	private int nLevel;
	private long lScore;
	private Bar bar;
	private Ball ball;
	private boolean bPlaying;
	private boolean bPaused;
	private boolean bDetectCollision = true;
	
	// These ArrayLists with capacities set
	private List<Movable> movDebris = new ArrayList<Movable>(300);
	private List<Movable> movFriends = new ArrayList<Movable>(100);
	private List<Movable> moveBlocks = new ArrayList<Movable>(200);

	private GameOpsList opsList = new GameOpsList();

	//added by Dmitriy
	private static Cc instance = null;

	private static final int MAX_LEVEL = 10;

	// Constructor made private - static Utility class only
	private Cc() {}

	public static Cc getInstance(){
		if (instance == null){
			instance = new Cc();
		}
		return instance;
	}


	public void initGame(){
		setLevel(1);
		setScore(0);
		setNumBalls(3);
		spawnBarAndBall(true);
	}
	
	// The parameter is true if this is for the beginning of the game, otherwise false
	public void spawnBarAndBall(boolean bFirst) {
		if (getNumBalls() != 0) {
			bar = new Bar();
			ball = new Ball(bar);
			//movFriends.enqueue(bar);
			opsList.enqueue(bar, CollisionOp.Operation.ADD);
			opsList.enqueue(ball, CollisionOp.Operation.ADD);
			if (!bFirst)
			    setNumBalls(getNumBalls() - 1);
		}

		//Sound from http://www.matthawkins.co.uk/sound_fx/weapons/missile_3.wav
		Sound.playSound("missile_3.wav");

	}


	//Getter & Setter for OpsList
	public GameOpsList getOpsList() {
		return opsList;
	}

	public void setOpsList(GameOpsList opsList) {
		this.opsList = opsList;
	}


	//Getter & Setter for detecting Collisions
	public boolean getDetectCollisions() {
		return bDetectCollision;
	}

	public void setDetectCollisions(boolean bDetect) {
		bDetectCollision = bDetect;
	}


	//Clear all
	public void clearAll(){
		movDebris.clear();
		movFriends.clear();
		moveBlocks.clear();
	}

	//Game over
	public boolean isGameOver() {		//if the number of falcons is zero, then game over
		if (getNumBalls() == 0) {
			return true;
		}
		return false;
	}


	//Getter & Setter for Playing status
	public boolean isPlaying() {
		return bPlaying;
	}

	public void setPlaying(boolean bPlaying) {
		this.bPlaying = bPlaying;
	}


	//Getter & Setter for Paused status
	public boolean isPaused() {
		return bPaused;
	}

	public void setPaused(boolean bPaused) {
		this.bPaused = bPaused;
	}


	//Getter & Setter for level
	public int getLevel() {	return nLevel;	}

	public void setLevel(int n) {
		if (n >MAX_LEVEL) {nLevel = MAX_LEVEL;}
		else { nLevel = n;}
	}

	//Getter & Setter for score
	public long getScore() {
		return lScore;
	}

	public void setScore(long lParam) {
		lScore = lParam;
	}

	//Getter & setter for Num of Balls left
	public int getNumBalls() {
		return nNumBall;
	}

	public void setNumBalls(int nParam) {
		nNumBall = nParam;
	}

	//Get bar, ball, ball angle
	public Bar getBar() { return bar; }

	public Ball getBall() { return ball;	}

	public double getBallAngle() {return getBall().getAngle();}


	//Get Lists
	public  List<Movable> getMovDebris() {
		return movDebris;
	}

	public  List<Movable> getMovFriends() {	return movFriends; }

	public  List<Movable> getMovBlocks() {
		return moveBlocks;
	}

}
