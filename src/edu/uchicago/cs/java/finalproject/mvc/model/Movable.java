package edu.uchicago.cs.java.finalproject.mvc.model;
import java.awt.*;

/**
 * Interface to make objects moveable. Moveable objects. have a center, a drawing point, a radius, and a team membership
 */

public interface Movable {

	public static enum Team {
		FRIEND, BLOCK, DEBRIS
	}

	//for the game to move and draw movable objects
	public void move();
	public void draw(Graphics g);

	//for collision detection
	public Point getCenter();
	public Point getDrawFrom();
	public double getRadius();
	public Team getTeam();


} //end Movable
