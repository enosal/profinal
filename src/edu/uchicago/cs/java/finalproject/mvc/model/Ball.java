package edu.uchicago.cs.java.finalproject.mvc.model;

import edu.uchicago.cs.java.finalproject.mvc.controller.Game;

import java.awt.*;

/**
 * Ball is the moving Shape used to hit the blocks
 */


public class Ball extends Shape {

	// ==============================================================
	// FIELDS
	// ==============================================================

	private final int RADIUS = 11;
	private double ballSpeed;
	private Point pntCenter;
	private Point pntTopLeft;
	private double dDeltaX;
	private double dDeltaY;
	private double dAngle;

	//Take the negative of THESE angles. That is the angle with the x axis of the bar going counter clockwise
	private static final int MID_ANGLE = -90;
	private static final int MIN_ANGLE = -45;
	private static final int MAX_ANGLE = -90; //Max angle would be MIN_ANGLE + MAX_ANGLE

	private static final double INITIAL_BALL_SPEED = 13;

	//moving booleans
	private boolean bMoving;
	private boolean bMovingUp;
	private boolean bMovingDown;

	//
	boolean bBallDying;

	//Bar
	private Bar bar;

	//Collision Detection
	private int SHIELD_LENGTH = 5;
	private boolean bShield;
	private int nShield;

	// ==============================================================
	// CONSTRUCTOR
	// ==============================================================

	public Ball(Bar bar){
		//call Shape constructor
		super();

		//set bar
		this.bar = bar;

		//set team
		setTeam(Team.FRIEND);

		//moving booleans
		bMoving = false;
		bMovingUp = false;
		bMovingDown = false;

		bBallDying = false;

		//initial ball speed
		ballSpeed = INITIAL_BALL_SPEED;

		//Points
		pntCenter = new Point();
		pntTopLeft = new Point();

		//Set TopLeft and Center points
		setDrawFrom(pntTopLeft);
		setCenter(getDrawFrom(), RADIUS);

		//Collision Detection
		Cc.getInstance().setDetectCollisions(true);
	}

	// ==============================================================
	// METHODS
	// ==============================================================

	//get angles
	public int getMIN_ANGLE() {	return MIN_ANGLE;}

	public int getMAX_ANGLE() {	return MAX_ANGLE;}

	//change directions- L&R
	public void changeDirLR() {	dDeltaX = -dDeltaX;	}

	//change directions - U&D
	public void changeDirUD() {	dDeltaY = -dDeltaY;}

	//change boolean up & down
	public void changeBoolUD() {
		bMovingDown = !bMovingDown;
		bMovingUp = !bMovingUp;
	}

	//hit block from top or bottom
	public void hitBlockUD() {
		setShield(SHIELD_LENGTH);
		changeDirUD();
		changeBoolUD();
	}

	//hit block from L or R
	public void hitBlockLR() {
		setShield(SHIELD_LENGTH);
		changeDirLR();
	}

	//getter & setter for shield
	public void setShield(int n) {nShield = n;}

	public int getShield() {return nShield;}


	//Start ball moving
	public void startMoving() {
		if (!bMoving) {
			bBallDying = false;
			bMoving = true;
			bMovingUp = true;
			dAngle = MID_ANGLE;
			setDeltaX(dAngle);
			setDeltaY(dAngle);
		}
	}

	//Stop ball from moving
	public void stopMoving() {
		bMoving = false;
		bMovingUp = false;
		bMovingDown = false;
	}

	//Change angle
	public void getNewAngle() {
		double ball_pos_along_bar = getCenter().x - bar.getDrawFrom().x;
		double position = 1 - (double) (ball_pos_along_bar / bar.getWidth());
		//bounce up
		if (position > .4 && position < .6)
			{dAngle = MID_ANGLE;}
		//cap left angle
		else if (position > 1)
			{dAngle = MIN_ANGLE + MAX_ANGLE;}
		//cap right angle
		else if (position < 0)
			{dAngle = MIN_ANGLE;}
		//determine angle based on where the ball hits the bar
		else
			{dAngle = MIN_ANGLE + position * MAX_ANGLE;}

		//set x and y displacement based off angle
		setDeltaX(dAngle);
		setDeltaY(dAngle);
	}

	//Get angle
	public double getAngle() {return dAngle;}

	//Getters & setters for displacement X & Y
	public double getDeltaY() {	return dDeltaY;	}

	public double getDeltaX() {return dDeltaX;	}

	public void setDeltaX(double angle) { dDeltaX = ballSpeed * Math.cos(Math.toRadians(dAngle));}

	public void setDeltaY(double angle) {dDeltaY = ballSpeed * Math.sin(Math.toRadians(dAngle));}


	//Override
	public Point getCenter() { return this.pntCenter;}

	public void setCenter(Point pnt, int rad) {
		this.pntCenter.x = pnt.x + rad;
		this.pntCenter.y = pnt.y + rad;
	}

	public void move() {
		super.move();
		setCenter(getDrawFrom(), RADIUS);
//		System.out.println("DRAW FROM: " + getDrawFrom().x + " , " + getDrawFrom().y + ". CENTER AT: " + getCenter().x + " , " + getCenter().y);

		if (bMoving) {
			//hits right wall
			if (getCenter().x + RADIUS >= Game.DIM.width) {
				changeDirLR();
				Cc.getInstance().setDetectCollisions(true);
			}
			//hits left wall
			else if (getCenter().x - RADIUS <= 0) {
				changeDirLR();
				Cc.getInstance().setDetectCollisions(true);
			}
			//hits top wall
			else if (getCenter().y - RADIUS <= 0) {
				//prevents ball from getting stuck at the top of the screen
				this.setCenter(new Point(getDrawFrom().x, 0), RADIUS);
				changeDirUD();
				changeBoolUD();
				Cc.getInstance().setDetectCollisions(true);
				System.out.println("X delta: " + dDeltaX);
				System.out.println("Y delta " + dDeltaY);
			}
			//hits bottom
			else if (getCenter().y - RADIUS>= Game.DIM.height - bar.getHeight()) {
				stopMoving();
				bBallDying = true;
				Cc.getInstance().setNumBalls(Cc.getInstance().getNumBalls() - 1);
			}
			//hits top of bar. BOUNCE
			else if
				(!bBallDying &&
					(getCenter().y + RADIUS >= (Game.DIM.height - 2*bar.getHeight())) && bMovingDown
					&&
					(
						(
							(getDrawFrom().x >= bar.getDrawFrom().x - 2*RADIUS) && (getDrawFrom().x <= (bar.getDrawFrom().x + bar.getWidth() + RADIUS))
						)
							||
						(
							((getDrawFrom().x >= bar.getDrawFrom().x - RADIUS) && (getDrawFrom().x <= (bar.getDrawFrom().x + bar.getWidth() + 2* RADIUS)))
						)
					)
				)
			{
				Cc.getInstance().setDetectCollisions(true);
				//get angle depending on where the ball hits the bar
				getNewAngle();

				//change dir
				changeBoolUD();
			}

			//redraw ball moving
			setDrawFrom(new Point((int) (getDrawFrom().x + getDeltaX()), (int) (getDrawFrom().y + getDeltaY())));
		}
		else {
			//ball on bar
			this.setDrawFrom(new Point(bar.getDrawFrom().x + bar.getWidth() / 2 - RADIUS, bar.getDrawFrom().y - 2 * RADIUS));
			Cc.getInstance().setDetectCollisions(true);
		}
	}

	public void draw(Graphics g) {
		if (nShield > 0) {
			setShield(getShield() - 1);
			Cc.getInstance().setDetectCollisions(false);
//			g.setColor(Color.CYAN);
			g.fillOval(getDrawFrom().x, getDrawFrom().y, 2 * RADIUS, 2 * RADIUS);
		}
		else if (nShield == 0) {
			Cc.getInstance().setDetectCollisions(true);
//			g.setColor(Color.magenta);
		}
		g.fillOval(getDrawFrom().x, getDrawFrom().y, 2 * RADIUS, 2 * RADIUS);
	}

	public double getRadius() {	return RADIUS;}

}