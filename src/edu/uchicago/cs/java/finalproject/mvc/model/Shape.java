package edu.uchicago.cs.java.finalproject.mvc.model;

import edu.uchicago.cs.java.finalproject.mvc.controller.Game;

import java.awt.*;
import java.util.ArrayList;

/**
 * Generic Shape class.
 */

public abstract class Shape implements Movable {
	// ==============================================================
	// FIELDS
	// ==============================================================

	//the center-point of this shape
	private Point pntCenter;

	//Draw Point for the shape
	private Point pntTopLeft;

	//this causes movement; change in x and change in y
	private double dDeltaX, dDeltaY;

	//every sprite needs to know about the size of the gaming environ
	private Dimension dim; //dim of the gaming environment

	//we need to know what team we're on
	private Team mTeam;

	//the radius of circumscribing circle
	private int nRadius;

	//the color of this sprite
	private Color col;

	// ==============================================================
	// CONSTRUCTOR
	// ==============================================================

	public Shape() {
		setDim(Game.DIM);
		setColor(Color.white);
		setDrawFrom(new Point(Game.R.nextInt(Game.DIM.width),
				Game.R.nextInt(Game.DIM.height)));
	}

	// ==============================================================
	// METHODS
	// ==============================================================

	public void setDim(Dimension dim) {	this.dim = dim;}

	public void move() {
		Point pnt = getDrawFrom();
		double dX = pnt.x + getDeltaX();
		double dY = pnt.y + getDeltaY();
	}


	//Getter & Setter for Team
	public Team getTeam() {
		//default
	  return mTeam;
	}

	public void setTeam(Team team){
		mTeam = team;
	}


	//Color getter & setter for Color
	public Color getColor() {
		return col;
	}

	public void setColor(Color col) {
		this.col = col;

	}


	//Getters & Setters for displacement X & Y. Hypotenuse
	public void setDeltaX(double dSet) {
		dDeltaX = dSet;
	}

	public void setDeltaY(double dSet) {
		dDeltaY = dSet;
	}

	public double getDeltaY() {
		return dDeltaY;
	}

	public double getDeltaX() {
		return dDeltaX;
	}

	protected double hypot(double dX, double dY) {
		return Math.sqrt(Math.pow(dX, 2) + Math.pow(dY, 2));
	}



	//Getter & Setters for Radius
	public double getRadius() {
		return nRadius;
	}

	public void setRadius(int n) {
		nRadius = n;

	}


	//Getter & Setter for Top Left (Drawing) point
	public void setDrawFrom(Point pnt) {
		pntTopLeft = pnt;
	}

	public Point getDrawFrom() {
		return pntTopLeft;
	}


	//Getter & Setter for Center (Collision) point
	public void setCenter(Point pnt) {
		pntCenter = pnt;
	}

	public Point getCenter() { return pntCenter; }


	//Draw
	public void draw(Graphics g) {
        g.setColor(getColor());
    }
    

}
